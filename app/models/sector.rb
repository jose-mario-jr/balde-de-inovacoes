class Sector < ApplicationRecord
  has_many :users

  has_many :campaign_sectors, dependent: :destroy
  has_many :campaigns, through: :campaign_sectors
end
