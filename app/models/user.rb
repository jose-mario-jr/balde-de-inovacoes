class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  belongs_to :sector
  has_many :answer

  enum role: {direction: 1, gerency: 2, coordination: 3, user: 4}
end
