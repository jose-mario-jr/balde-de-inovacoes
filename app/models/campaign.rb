class Campaign < ApplicationRecord
  enum medium: {bot: 1, form: 2}

  has_many :campaign_sectors, dependent: :destroy
  has_many :sectors, through: :campaign_sectors

  has_many :answers
end
