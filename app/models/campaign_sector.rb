class CampaignSector < ApplicationRecord
  belongs_to :campaign
  belongs_to :sector
end
