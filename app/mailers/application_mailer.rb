class ApplicationMailer < ActionMailer::Base
  default from: "jose.mario.jr.42@gmail.com"
  layout "mailer"

  def campaign_email
    @campaign = params[:campaign]
    @user = params[:user]
    mail(to: @user.email, subject: 'Você recebeu um convite de responder a campanha do balde amarelo!')
  end  
end
