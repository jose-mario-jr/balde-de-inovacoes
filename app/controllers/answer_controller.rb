class AnswerController < ApplicationController

  def index
    @campaign = Campaign.find(params[:campaign_id])
  end

  def new 
    @campaign = Campaign.find(params[:campaign_id])
    @user = User.find(params[:user_id])
  end

  def create
    @answer = Answer.new(answer_params)
    
    if @answer.save
      respond_to do |format|
        format.html { redirect_to answer_index_path(campaign_id: @answer.campaign.id) }
      end
    else
      puts @answer.errors.full_messages
      respond_to do |format|
        format.html { redirect_to campaign_index_path, alert: "Error creating Campaign." }
      end
    end
  end

  private
    def answer_params
      params.require(:answer).permit(:text, :campaign_id, :user_id)
    end
end
