class CampaignController < ApplicationController
  before_action :authenticate_user!

  def index
    @campaigns = Campaign.all
  end

  def show
    @campaign = Campaign.find(params[:id])
  end

  def create
    @campaign = Campaign.new(campaign_params)
    if @campaign.save
      if @campaign.medium == 'form'
        recepient = @campaign.sectors.map { |s| s.users }.flatten
        recepient.each do |r| 
          ApplicationMailer.with(campaign: @campaign, user: r).campaign_email.deliver_now
        end
      else
        puts "WIP: connect to bot"
      end
      respond_to do |format|
        format.html { redirect_to campaign_path(@campaign.id) }
      end
    else
      respond_to do |format|
        format.html { redirect_to campaign_path, alert: "Error creating Campaign." }
      end
    end
  end

  private
    def campaign_params
      _params = params.require(:campaign).permit(:name, :text, :medium, :sector_ids)
      _params[:sector_ids] = _params[:sector_ids].split(',')
      return _params
    end
end
