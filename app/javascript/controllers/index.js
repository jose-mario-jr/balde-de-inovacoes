// Import and register all your controllers from the importmap under controllers/*

import { application } from "./application"

import CheckMultipleController from "./check_multiple_controller";
application.register("check-multiple", CheckMultipleController);
