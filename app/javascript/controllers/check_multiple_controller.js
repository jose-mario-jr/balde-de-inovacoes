import { Controller } from '@hotwired/stimulus'
console.log('aaaaaaaaa')

export default class extends Controller {
  static targets = ['hiddenField']

  toggle() {
    const vals = [
      ...document.querySelectorAll(
        '.checkbox-wrapper input[type="checkbox"]:checked'
      ),
    ].map(e => e.value)
    this.hiddenFieldTarget.value = vals.join(',')
  }
}
