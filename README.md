# Balde de Inovações

## Prints
### Login
![Login](https://imgur.com/w5FEUet.png)
### Tela principal
![Tela principal](https://i.imgur.com/tONZ8LV.png)
### Nova Campanha
![Tela principal](https://imgur.com/5DUrQQU.png)
### Responder Campanha
![Responder campanha](https://imgur.com/6il7myp.png)

### Respostas da Campanha
![Respostas da Campanha](https://imgur.com/o6D2szz.png)

# Run

    bin/dev

---

By BioGrow Innovators
