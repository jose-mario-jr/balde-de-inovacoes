# This file should ensure the existence of records required to run the application in every environment (production,
# development, test). The code here should be idempotent so that it can be executed at any point in every environment.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Example:
#
#   ["Action", "Comedy", "Drama", "Horror"].each do |genre_name|
#     MovieGenre.find_or_create_by!(name: genre_name)
#   end

sectors = ["Manutenção", "TI", "RH", "Comercial"]
sectors.each do |s|
  Sector.create(name: s) if Sector.where(name: s).blank?
end

User.create(email: 'jmsj777@hotmail.com', name:'jmsj777@hotmail.com', password: "jmsj777@hotmail.com", address: "telegramAddress", role: 1, sector: Sector.first) if User.where(email: 'jmsj777@hotmail.com').blank?
User.create(email: 'jose.mario.jr@outlook.com', name:'jose.mario.jr@outlook.com', password: "jose.mario.jr@outlook.com", address: "telegramAddress", role: 1, sector: Sector.first) if User.where(email: 'jose.mario.jr@outlook.com').blank?
