class AddCampaign < ActiveRecord::Migration[7.1]
  def change
    create_table :sectors do |t|
      t.string :name
    end
    create_table :campaigns do |t|
      t.string :name
      t.integer :medium
      t.timestamps null: false
    end
    change_table :users do |t|
      t.string :name
      t.string :address
      t.integer :role
      t.references :sector, foreign_key: true
    end
    create_table :campaign_sectors do |t|
      t.references :campaign, null: false, foreign_key: true
      t.references :sector, null: false, foreign_key: true
    end
  end
end
