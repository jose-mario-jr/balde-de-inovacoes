class AddAnswerTable < ActiveRecord::Migration[7.1]
  def change
    create_table :answers do |t|
      t.string :text
      t.references :campaign, null: true, foreign_key: true
      t.references :user, null: true, foreign_key: true
    end
  end
end
