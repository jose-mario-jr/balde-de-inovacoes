class AddTExtToCampaign < ActiveRecord::Migration[7.1]
  def change
    change_table :campaigns do |t|
      t.string :text
    end
  end
end
